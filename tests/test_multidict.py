__author__ = 'jstubbs'


from werkzeug.datastructures import MultiDict
import requests

import sys
sys.path.append('/home/jstubbs/bitbucket-repos/apim-python-sdk/')

from apim.apim import Apim


def main():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jstubbs@test.org', password='p@ssword')
    apim.authenticate_to_publisher()

    params = []
    params.append(('action', 'addAPI'))
    params.append(('name', 'File3'))
    params.append(('context', '/file3'))
    params.append(('version', 'v2'))

    params.append(('description', 'FilesAPI'))
    params.append(('tags', 'Files,api'))
    params.append(('tier', 'Unlimited'))
    params.append(('tier', 'Gold'))
    params.append(('tier', 'Silver'))
    params.append(('tier', 'Bronze'))

    params.append(('transports', 'http'))
    params.append(('http_checked', 'http'))
    params.append(('transports', 'https'))
    params.append(('https_checked', 'https'))

    params.append(('resourceCount', '0'))
    params.append(('resourceMethod-0', 'GET,POST,PUT,DELETE,OPTIONS'))
    params.append(('resourceMethodAuthType-0',
                   'Application & Application User,Application & Application User,Application & Application User,Application & Application User,None'))

    params.append(('uriTemplate-0', '/*'))
    params.append(('resourceMethodThrottlingTier-0',
                   'Unlimited,Unlimited,Unlimited,Unlimited,Unlimited'))
    params.append(('tiersCollection', 'Unlimited,Gold,Silver,Bronze'))
    params.append(('endpoint_config',
                   '{"production_endpoints":{"url":"http://file3.production.agave.tacc.utexas.edu:8080/file3","config":null},"endpoint_type":"http"}'))

    data = MultiDict(params)
    url = apim.publisher_add_api_url
    import pdb; pdb.set_trace()
    rsp = requests.post(url, data=data, cookies=apim.publisher_cookies, verify=apim.verify)
    print str(rsp), rsp.content

if __name__ == "__main__":
    main()