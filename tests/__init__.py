#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tests for the apim-python-sdk based on the pytest framework. Run all the tests for this package with:
    py.test tests/

or run an individual module's tests with, e.g.:
    py.test tests/test_apim.py

"""