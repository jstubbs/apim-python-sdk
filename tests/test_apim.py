__author__ = 'jstubbs'

from apim.apim import Apim
from apim.exceptions import ApimException
import pytest

def test_create_apim_tenant_admin():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jstubbs@test.org', password='p@ssword')
    assert(apim.store_services_base_url == 'https://dev1.agave.tacc.utexas.edu/store/site/blocks')
    assert(apim.publisher_services_base_url == 'https://dev1.agave.tacc.utexas.edu/publisher/site/blocks')

def test_login_tenant_admin():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jstubbs@test.org', password='p@ssword')
    try:
        apim.authenticate_to_store()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.store_cookies)
    try:
        apim.authenticate_to_publisher()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.publisher_cookies)

def test_login_tenant_user():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jdoe@test.org', password='abcde')
    try:
        apim.authenticate_to_store()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.store_cookies)

def test_get_clients():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jdoe@test.org', password='abcde')
    try:
        clients = apim.get_clients()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.store_cookies)
    assert(len(clients) > 0)
    for client in clients:
        assert(hasattr(client, 'name'))
        assert(hasattr(client, 'tier'))
        assert(hasattr(client, 'description'))
        assert(hasattr(client, 'callback_url'))

def test_get_published_apis():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jdoe@test.org', password='abcde')
    try:
        apis = apim.get_published_apis()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.store_cookies)
    assert(len(apis) > 0)
    found = False
    for api in apis:
        assert(hasattr(api, 'name'))
        assert(hasattr(api, 'context'))
        assert(hasattr(api, 'status'))
        assert(hasattr(api, 'provider'))
        if api.name == 'test':
            found = True
            assert(api.context == '/test')
    assert(found)

def test_get_all_apis():
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jstubbs@test.org', password='p@ssword')
    try:
        apis = apim.get_all_apis()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    assert(apim.publisher_cookies)
    assert(len(apis) > 0)
    found = False
    for api in apis:
        assert(hasattr(api, 'name'))
        assert(hasattr(api, 'context'))
        assert(hasattr(api, 'status'))
        assert(hasattr(api, 'provider'))
        if api.name == 'test':
            found = True
    assert(found)

