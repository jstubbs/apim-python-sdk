__author__ = 'jstubbs'

import pytest
import uuid

from apim.api import Api, ApiResource, ResourceMethod
from apim.apim import Apim
from apim.exceptions import ApimException


def test_create_get_resource_method():
    resource_method = ResourceMethod('GET', 'Application & Application User', 'Unlimited')
    assert(resource_method.name == 'GET')
    assert(resource_method.auth == 'Application & Application User')
    assert(resource_method.throttling_tier == 'Unlimited')

def test_create_default_api_resource():
    default_resource = ApiResource()
    assert(default_resource.get_method_names() == 'GET,POST,PUT,DELETE,OPTIONS')
    assert(default_resource.get_method_auths() == 'Application & Application User,Application & Application User,Application & Application User,Application & Application User,None')
    assert(default_resource.get_method_throttling_tiers() == 'Unlimited,Unlimited,Unlimited,Unlimited,Unlimited')

def test_create_and_publish_api():
    resources = [ApiResource()]
    name = 'test-{}'.format(str(uuid.uuid4()))[:12]
    context = '/' + name
    apim = Apim(base_url='https://dev1.agave.tacc.utexas.edu', username='jstubbs@test.org', password='p@ssword')
    api = Api(apim=apim,
              name=name,
              context=context,
              version='v2',
              resources=resources,
              tags=['testgenerated','api'],
              production_endpoint='http://test.dev.agave.tacc.utexas.edu:8080',
              description='testAPI')
    assert(api.name == name)
    assert(api.context == context)
    assert(api.version == 'v2')
    assert(api.resources[0].get_method_names() == 'GET,POST,PUT,DELETE,OPTIONS')
    assert(api.resources[0].get_method_auths() == 'Application & Application User,Application & Application User,Application & Application User,Application & Application User,None')
    assert(api.resources[0].get_method_throttling_tiers() == 'Unlimited,Unlimited,Unlimited,Unlimited,Unlimited')
    assert(api.production_endpoint == 'http://test.dev.agave.tacc.utexas.edu:8080')
    assert(api.description == 'testAPI')
    try:
        api.create()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    print "Api created: ", name

    api = Api(apim=apim,
              name=name,
              version='v2',
              provider='jstubbs@test.org',
              status='CREATED')
    try:
        api.publish()
    except ApimException as e:
        print "ApimException: ", e.message
        raise e
    print "Api published: ", name

