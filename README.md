# WSO2 API Manager Python SDK #



## Overview ##

This project provides Python objects and interfaces to the WSO2 API Manager platform. It wraps the Publisher and
Store APIs with enhancements and other improvements. Example uses include to create APIs, applications, and
subscriptions, and manage these objects.




## Installation ##

Install from source: Clone the repository, change into the project directory, activate a
virtualenv if appropriate and run:

```
#!bash

$ python setup.py install
```

All dependencies will be automatically installed along with apim. Confirm your installation:

Run the tests
```
#!bash

$ py.test tests/
```

Import the Apim class
```
#!python

>>> from apim.apim import Apim
```



## Quickstart ##

Instances of the Apim class represent an authenticated user of a specific WSO2 Api Manager
instance. Create an Apim object by passing the base url for an instance of WSO2 APIM and the
username and password for an account there

```
#!python

>>> from apim.apim import Apim
>>> apim = Apim('https://dev1.agave.tacc.utexas.edu', username='jdoe@test.org', password='abcde')
```

Get the published APIs:
```
#!python

>>> apis = apim.get_published_apis()
```

these are api.Api objects:
```
#!python

>>> apis[0]
<apim.api.Api object at 0x272fdd0>
```

Print the names:
```
#!python

>>> for api in apis: print api.name
Apps
Jobs
Meta
Monitors
Notifications
Postits
Profiles
Systems
test
test-084333b
test-55f9dac
Transforms
```

Get jdoe's clients and print the names:
```
#!python

>>> clients = apim.get_clients()
>>> for client in clients: print client.name
u'DefaultApplication'
```