__author__ = 'jstubbs'

Config = {
    'default_client_tier': 'Unlimited',
    'default_api_tiers': ['Unlimited', 'Gold', 'Silver', 'Bronze'],
    'default_api_transports': ['http', 'https'],
    'default_api_methods': [{'name': 'GET', 'auth':'Application & Application User', 'throttling_tier': 'Unlimited'},
                {'name': 'POST', 'auth':'Application & Application User', 'throttling_tier': 'Unlimited'},
                {'name': 'PUT','auth':'Application & Application User', 'throttling_tier': 'Unlimited'},
                {'name': 'DELETE', 'auth':'Application & Application User', 'throttling_tier': 'Unlimited'},
                {'name': 'OPTIONS', 'auth':'None', 'throttling_tier': 'Unlimited'}],

}