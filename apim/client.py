"""
Class for working with WSO2 clients (applications)
"""

from config import Config

class Client(object):

    def __init__(self,
                 # the apim instance for this client
                 apim,

                 # required client attributes
                 name,

                 # optional client attributes
                 tier=Config.get('default_client_tier'),
                 description=None,
                 callback_url=None,

                 # these parameters will be added when the client is registered in the store.
                 id=None,
                 consumer_key=None,
                 consumer_secret=None,
                 status=None):

        self.apim = apim
        self.name = name
        self.tier = tier
        self.description = description
        self.callback_url = callback_url
        self.id = id
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.status = status

    def register(self):
        """Register a client in the API Store."""
        pass

    def remove(self):
        """Removes a client from the API Store."""
        pass

    def get_details(self):
        """Retrieve additional client details directly from MySQL tables."""
        pass

    @staticmethod
    def deserialize(apim, obj):
        """Deserialize a json description to a client object."""

        name = obj.get('name')
        tier = obj.get('tier')
        description = obj.get('description')
        callback_url = obj.get('callbackUrl')
        id = obj.get('id')
        status = obj.get('status')
        client = Client(apim, name, tier, description, callback_url, id, status)
        client.get_details()
        return client