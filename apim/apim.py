"""
An instance of WSO2 API Manager.
"""

import requests

from client import Client
from api import Api
from exceptions import ApimException

class Apim(object):
    """
    Represents an authenticated user to a specific (tenant of an) APIM instance.
    """
    def __init__(self, base_url, username=None, password=None, verify=False):
        self.base_url = base_url
        self.store_services_base_url = self.base_url + '/store/site/blocks'
        self.store_auth_url = self.store_services_base_url + '/user/login/ajax/login.jag'
        self.store_apps_url = self.store_services_base_url + '/application/application-list/ajax/application-list.jag'
        self.store_apis_url = self.store_services_base_url + '/api/listing/ajax/list.jag?action=getAllPaginatedPublishedAPIs&start=0&end=99999&'
        self.add_app_url = self.store_services_base_url + '/application/application-add/ajax/application-add.jag'
        self.remove_app_url = self.store_services_base_url + '/application/application-remove/ajax/application-remove.jag'

        self.publisher_services_base_url = self.base_url + '/publisher/site/blocks'
        self.publisher_apis_url = self.publisher_services_base_url + '/listing/ajax/item-list.jag'
        self.publisher_auth_url = self.publisher_services_base_url + '/user/login/ajax/login.jag'
        self.publisher_add_api_url = self.publisher_services_base_url + '/item-add/ajax/add.jag'
        self.publisher_update_api_status_url = self.publisher_services_base_url + '/life-cycles/ajax/life-cycles.jag'
        self.verify = verify
        
        self.username = username
        self.password = password

        self.tenant = self._get_tenant()
        self.store_cookies = None
        self.publisher_cookies = None

    def _get_tenant(self):
        if len(self.username.split('@')) > 1:
            return self.username.split('@')[1]
        else:
            return None

    def _auth_request(self, url):

        data = {'action':'login',
                'username': self.username,
                'password': self.password}
        try:
            rsp = requests.post(url, data, verify=self.verify)
        except Exception as e:
            raise ApimException(str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to authenticate user; status code: "
                        + str(rsp.status_code) + "msg:" + str(rsp.content))
        if rsp.json().get("error"):
            if rsp.json().get("message"):
                raise ApimException(rsp.json().get("message").strip())
            raise ApimException("Invalid username/password combination.")
        return rsp.cookies

    def authenticate_to_store(self):
        """Authenticate a WSO2 user to the API Store and set the cookies for the session."""

        self.store_cookies = self._auth_request(self.store_auth_url)

    def authenticate_to_publisher(self):
        """Authenticate a WSO2 user to the APIM Publisher; return cookies set on the session."""

        self.publisher_cookies = self._auth_request(self.publisher_auth_url)

    def decode_jwt(self):
        """Verify the signature on a JWT and return the decoded profile if successful."""
        pass

    def get_clients(self):
        """Returns the list of clients owned by the user."""

        self.authenticate_to_store()
        url = self.store_apps_url
        params = {'action': 'getApplications'}
        try:
            rsp = requests.get(url, cookies=self.store_cookies, params=params, verify=self.verify)
        except Exception as e:
            raise ApimException("Unable to retrieve clients; " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to retrieve clients; status code:" +
                        str(rsp.status_code))
        if not rsp.json().get("applications"):
            raise ApimException("Unable to retrieve clients; content: " + str(rsp.content))
        apps = rsp.json().get("applications")
        return [Client.deserialize(self, app) for app in apps]

    def get_all_apis(self):
        """Returns the list of all APIs in the Publisher."""

        self.authenticate_to_publisher()
        url = self.publisher_apis_url + "?action=getAllAPIs"
        try:
            rsp = requests.get(url, cookies=self.publisher_cookies, verify=self.verify)
        except Exception as e:
            raise ApimException("Unable to retrieve APIs; " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to retrieve APIs; status code:" +
                        str(rsp.status_code))
        if rsp.json().get("error"):
            raise ApimException("Error returned from publisher:" +
                        str(rsp.content))
        apis = rsp.json().get('apis')
        return [Api.deserialize(self, api) for api in apis]

    def get_published_apis(self, tenant=None):
        """Returns a list of all published APIs available to the user. Defaults to the user's tenant."""

        self.authenticate_to_store()
        url = self.store_apis_url
        if tenant:
            url = url + 'tenant=' + tenant
        elif self.tenant:
            url = url + 'tenant=' + self.tenant
        try:
            rsp = requests.get(url, cookies=self.store_cookies, verify=self.verify)
        except Exception as e:
            raise ApimException("Unable to retrieve APIs; " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to retrieve APIs; status code:" +
                        str(rsp.status_code))
        apis = rsp.json().get('apis')
        return [Api.deserialize(self, api) for api in apis]
