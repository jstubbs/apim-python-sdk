"""
Classes for working with an API in a WSO2 API Manager instance.
"""

import requests
from werkzeug.datastructures import MultiDict

from exceptions import ApimException
from config import Config


class ResourceMethod(object):
    """
    Represents an HTTP method of an ApiResource including the authentication and throttling tier
    for that method.
    """

    def __init__(self, name, auth, throttling_tier):
        self.name = name
        self.auth = auth
        self.throttling_tier = throttling_tier


DEFAULT_METHODS = [ResourceMethod(method['name'], method['auth'], method['throttling_tier'])
                   for method in Config.get('default_api_methods')]


class ApiResource(object):
    """
    Represents a single endpoint within an API.
    """
    def __init__(self,
                 uri_template='/*',
                 methods=DEFAULT_METHODS):

        self.uri_template = uri_template
        self.methods = methods
        self.method_names = [method.name for method in self.methods]
        self.method_auths = [method.auth for method in self.methods]
        self.method_throttling_tiers = [method.throttling_tier for method in self.methods]

    def get_method_names(self):
        return ','.join(self.method_names)

    def get_method_auths(self):
        return ','.join(self.method_auths)

    def get_method_throttling_tiers(self):
        return ','.join(self.method_throttling_tiers)

class Api(object):

    def __init__(self,
                # the apim instance for this api
                apim,

                # required api attributes:
                name,
                version,
                provider=None,
                status="new",

                # optional api attributes
                production_endpoint=None,
                context=None,
                resources=None,
                endpoint_type = 'http',
                tiers=Config.get('default_api_tiers'),
                transports=Config.get('default_api_transports'),
                visibility='public API',
                tags=None,
                description=None,
                sandbox_endpoint=None,):
        """
        Construct an API object from the provided attributes.

        If ``status`` is not new, attempts to retrieve additional attributes from publisher.

        """
        self.apim = apim
        self.name = name
        self.set_context(context)
        self.version = version
        self.status = status
        self.provider = provider

        # for api's already in the publisher, look up details:
        if not status == 'new':
            self._get_details()
        else:
            self.resources = resources
            self.production_endpoint = production_endpoint

            self.endpoint_type = endpoint_type
            self.tiers = tiers
            self.transports = transports
            self.visibility = visibility
            self.tags = tags
            self.description = description
            self.sandbox_endpoint = sandbox_endpoint

    def _get_details(self):
        try:
            self.apim.authenticate_to_publisher()
        except ApimException:
            # if the user does not have access to the publisher, return the Api object as is,
            return
        url = self.apim.publisher_apis_url
        data = {'action': 'getAPI',
                'name': self.name,
                'provider': self.provider,
                'version': self.version,}
        try:
            rsp = requests.post(url, data=data, cookies=self.apim.publisher_cookies,
                                verify=self.apim.verify)
        except Exception as e:
            raise ApimException("Unable to get details for API " + self.name + "; message: " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to get details for API " + self.name +
                        "; status code: " + str(rsp.status_code))
        if rsp.json().get('error'):
            raise ApimException("Unable to get details for API " + self.name + " content: " + str(rsp.content))
        self._update_from_json(rsp.json().get('api'))

    def _update_from_json(self, obj):
        if obj.get('context'):
            self.set_context(obj.get('context'))
        if obj.get('description'):
            self.description = obj.get('description')

    def _build_create_req_body(self):
        params = []
        params.append(('action', 'addAPI'))
        params.append(('name', self.name))
        params.append(('context', self.context))
        params.append(('version', self.version))

        params.append(('description', self.description))
        params.append(('tags', ','.join(self.tags)))

        for tier in self.tiers:
            params.append(('tier', tier))
        for transport in self.transports:
            params.append(('transports', transport))
            params.append((transport + '_checked', transport))

        params.append(('resourceCount', str(len(self.resources)-1)))

        for idx, resource in enumerate(self.resources):
            params.append(('resourceMethod-' + str(idx), resource.get_method_names()))
            params.append(('resourceMethodAuthType-' + str(idx), resource.get_method_auths()))
            params.append(('uriTemplate-' + str(idx), resource.uri_template))
            params.append(('resourceMethodThrottlingTier-' +
                           str(idx), resource.get_method_throttling_tiers()))

        params.append(('tiersCollection', self.get_tiers()))
        params.append(('endpoint_config',
                       '{"production_endpoints":{"url":"' +
                       self.production_endpoint + '","config":null},"endpoint_type":"' +
                       self.endpoint_type + '"}'))
        return MultiDict(params)

    def set_context(self, context):
        if hasattr(context, 'split') and len(context.split('/t/'+ self.apim.tenant)) > 1:
            context = context.split('/t/'+ self.apim.tenant)[1]
        self.context = context

    def get_tiers(self):
        return ','.join(self.tiers)

    def create(self):
        """Create an API in the API Publisher."""

        self.apim.authenticate_to_publisher()
        url = self.apim.publisher_add_api_url
        data = self._build_create_req_body()
        try:
            rsp = requests.post(url, data=data, cookies=self.apim.publisher_cookies,
                                verify=self.apim.verify)
        except Exception as e:
            raise ApimException("Unable to create API " + self.name + "; message: " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to create API " + self.name +
                        "; status code: " + str(rsp.status_code))
        if rsp.json().get('error'):
            raise ApimException("Unable to create API " + self.name + " content: " + str(rsp.content))

    def update_status(self, status, publish_to_gateway=True):
        """Update the status of an api.

        ``status`` is the new status of the api.

        """
        self.apim.authenticate_to_publisher()
        url = self.apim.publisher_update_api_status_url
        data = {'action': 'updateStatus',
                'name': self.name,
                'version': self.version,
                'provider': self.provider,
                'status': status,
                'publishToGateway': publish_to_gateway}
        try:
            rsp = requests.post(url, data=data, cookies=self.apim.publisher_cookies,
                            verify=self.apim.verify)
        except Exception as e:
            raise ApimException("Unable to update status of API " + self.name +
                                "; message: " + str(e))
        if not rsp.status_code == 200:
            raise ApimException("Unable to update status of API " + self.name +
                        "; status code: " + str(rsp.status_code))
        if rsp.json().get('error'):
            raise ApimException("Unable to update status of API " + self.name +
                                " content: " + str(rsp.content))

    def publish(self):
        """Update status of an api to published."""

        self.update_status("PUBLISHED")

    @staticmethod
    def deserialize(apim, obj):
        """
        Returns an Api object from a JSON description.

        ``obj`` is a string containing the JSON description.

        """
        name = obj.get('name')
        provider = obj.get('provider')
        version = obj.get('version')
        status = obj.get('status')
        api = Api(apim,
                  name,
                  version,
                  provider=provider,
                  status=status)
        api._update_from_json(obj)
        return api